#Задача 4.1
def div3():
    while True:
        try:
            number = int(input("Введите число: "))
            if number % 3 == 0:
                return True
            else:
                return False
        except ValueError:
            print("Ошибка: введите целое число.")

result = div3()

if result:
    print("Число делится на 3")
else:
    print("Число не делится на 3")

# Задача 4.2

def divide():
    while True:
        try:
            number = float(input("Введите число: "))
            result = 100 / number
            return result
        except ValueError:
            print("Ошибка: введите число.")
        except ZeroDivisionError:
            print("Ошибка: нельзя делить на ноль.")

result = divide()
print(f"Результат деления: {result}")

#Задача 4.3

def is_magic_date():
    date_string = input("Введите дату в формате ДД.ММ.ГГГГ: ")
    d, m, y = map(int, date_string.split('.'))
    last_two_digits_of_year = y % 100
    return d * m == last_two_digits_of_year

print(is_magic_date())

#Задача 4.4

def isHappyTicket(number):
    if not isinstance(number, str):  # Проверка, что аргумент строка
        return 'Номер должен быть строкой'
    elif not number.isdigit():  # Проверка на текст
        return 'Номер не может содержать буквы и специальные символы'
    elif len(number) % 2 != 0:  # Проверка на четность количества цифр в номере
        return 'Количество цифр в номере билета должено быть четным!'

    sum = 0
    for i in range(len(number) // 2):
        sum += int(number[i]) - int(number[-i - 1])
    return sum == 0

print(isHappyTicket('123321'))
