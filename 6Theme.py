
# Задание 6.1
capitals = {"Австрия": "Вена", "Бельгия": "Брюссель", "Великобритания": "Лондон",
            "Германия": "Берлин", "Ирландия": "Дублин", "Лихтенштейн": "Вадуц",
            "Нидерладны": "Амстердам", "Франция": "Париж", "Белоруссия": "Минск",
            "Болгария": "София", "Польша": "Варшава", "Чехия": "Прага",
            "Албания": "Тирана", "Босния и Герцеговина": "Сараево",
            "Северная Македония": "Скопье", "Сербия": "Белград"}

for country, capital in capitals.items():
    print(country + ": " + capital)

print("Столица Германии: " + capitals["Германия"])

sort_c = sorted(capitals.items())

for country, capital in sort_c:
    print(country + ": " + capital)

# Задание 6.2
alph = {
        "а": 1,
        "б": 3,
        "в": 1,
        "г": 3,
        "д": 2,
        "е": 1,
        "ё": 3,
        "ж": 5,
        "з": 5,
        "и": 1,
        "й": 4,
        "к": 2,
        "л": 2,
        "м": 2,
        "н": 1,
        "о": 1,
        "п": 2,
        "р": 1,
        "с": 1,
        "т": 1,
        "у": 2,
        "ф": 10,
        "х": 5,
        "ц": 5,
        "ч": 5,
        "ш": 8,
        "щ": 10,
        "ъ": 10,
        "ы": 4,
        "ь": 3,
        "э": 8,
        "ю": 8,
        "я": 3
}


    word = input("Введите слово: ").lower()
    score = sum([alph.get(letter, 0) for letter in word])
    print("Стоимость слова:", score)

    # Задание 6.3

students = [
    {"name": "Никита", "languages": ["Английский", "Французкий", "Немецкий"]},
    {"name": "Оля", "languages": ["Испанский", "Китайский"]},
    {"name": "Вася", "languages": ["Китайский", "Японский"]},
    {"name": "Боря", "languages": ["Английский", "Испанский"]}
]

all_languages = set()

ch_speak = set()

for student in students:
    all_languages.update(student["languages"])

    if "Китайский" in student["languages"]:
        ch_speak.add(student["name"])

sort_lang = sorted(list(all_languages))

print("Список всех языков:", sort_lang)
print("Знающих китайский язык:", ch_speak)
