#Задание A
password = input("Введите пароль: ")
confirm_password = input("Подтвердите пароль: ")

if password == confirm_password:
    print("Пароль принят")
else:
    print("Пароль не принят")
#Задание Б

seat_number = int(input("Введите номер места (от 1 до 54): ")) # Получение номера места от пользователя

# Проверка, что номер места находится в допустимом диапазоне
if seat_number < 1 or seat_number > 54:
    print("Ошибка: введен некорректный номер места.")
else:
    # Определение типа места
    if seat_number in range(1, 37):  # места в купе
        if seat_number % 4 in (1, 4):  # проверка на верхнее/нижнее место
            seat_type = "верхнее"
        else:
            seat_type = "нижнее"
        carriage_type = "купе"
    else:  # боковые места
        if seat_number % 2 == 1:  # проверка на верхнее/нижнее место
            seat_type = "верхнее"
        else:
            seat_type = "нижнее"
        carriage_type = "боковое"

    # Вывод результата
    print(f"Место {seat_number} является {seat_type} местом в {carriage_type} месте.")
# Задание В

def is_leap_year(year):
    if year % 4 == 0 and (year % 100 != 0 or year % 400 == 0):
        print(f"{year} год  - високосный")
     else:
        print("Это год не високосный")

# Задание Г
color1 = input("Введите первый цвет: ")
color2 = input("Введите второй цвет: ")

if color1 == "красный" and color2 == "синий" or color1 == "синий" and color2 == "красный":
    secondary_color = "фиолетовый"
elif color1 == "красный" and color2 == "желтый" or color1 == "желтый" and color2 == "красный":
    secondary_color = "оранжевый"
elif color1 == "синий" and color2 == "желтый" or color1 == "желтый" and color2 == "синий":
    secondary_color = "зеленый"
else:
    print("Ошибка: введите два основных цвета (красный, синий или желтый)")
    secondary_color = None

if secondary_color:
    print(f"При смешивании {color1} и {color2} получится {secondary_color}")

 #Задание Д
n = int(input("Введите количество слов: "))

result = ""
for i in range(n):
    word = input(f"Введите слово {i + 1}: ")
    result += word + " "

print("Результат:", result.strip())