
#Задача 3.1 выполнена в предыдущих задания, ниже представлено её модификация остановкой словом "stop"
#Задача 3.2

result = ""
while True:
    word = input("Введите слово: ")
    if word == "stop":
        break
    result += word + " "

print("Результат:", result.strip())

#Задача 3.3

word = input("Введите слово: ")
if 'ф' in word.lower():
    print("Ого! Это редкое слово!")
else:
    print("Эх, это не очень редкое слово...")

#Задача 3.4

import random

correct_answers = 0
wrong_answers = 0

while wrong_answers < 3:
    a = random.randint(1, 10)
    b = random.randint(1, 10)
    expr = f"{a} + {b} = "
    user_answer = input(expr)
    if int(user_answer) == a + b:
        print("Правильно!")
        correct_answers += 1
    else:
        print("Ответ неверный")
        wrong_answers += 1

print(f"Игра окончена. Правильных ответов: {correct_answers}")