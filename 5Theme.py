#Задача 5.1

import random

list = random.sample(range(1, 101), 5)

input_num = int(input("Введите число: "))

if input_num in list:
    print("Поздравляю, Вы угадали число!")
else:
    print("Нет такого числа!")

print("Исходный список:", list)

#Задача 5.2

list = [1, 3, 5, 3, 2, 7, 5]

d = set([x for x in list if list.count(x) > 1])

if d:
    print("Повторяющиеся элементы:", d)
else:
    print("В списке нет повторяющихся элементов.")

#Задача 5.3

week_d = ('Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота', 'Воскресенье')

weekend_count = int(input("Сколько выходных дней на этой неделе? "))

weekend_days = week_d[-weekend_count:]
work_days = week_d[:len(week_d) - weekend_count]

print("Ваши выходные дни:", ", ".join(weekend_days))
print("Ваши рабочие дни:", ", ".join(work_days))

#Задача 5.4

group1 = ['Иванов', 'Петров', 'Сидоров', 'Кузнецов', 'Морозов', 'Соловьев', 'Волков', 'Никитин', 'Горбунов', 'Козлов']
group2 = ['Смирнов', 'Попов', 'Ковалев', 'Соколов', 'Михайлов', 'Федоров', 'Комаров', 'Исаев', 'Крылов', 'Жуков']

team = tuple(group1[:5] + group2[:5])

print("Группа 1: ", group1)
print("Группа 2: ", group2)
print("Команда: ", team)

print("Длина команды: ", len(team))

sorted_team = sorted(team)

print("Отсортированная команда: ", sorted_team)

ivanov_count = sorted_team.count("Иванов")

if ivanov_count > 0:
    print("Студент Иванов в команде. Его фамилия встречается", ivanov_count, "раз(а).")
else:
    print("Студент Иванов не входит в команду.")